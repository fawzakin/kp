#!/usr/bin/env python
from chatbot.chatgpt import get_response
from chatbot.twitter import post_twitter
from chatbot.thread import write_input
from chatbot.discord import send_discord
from chatbot.driver import create_driver
from dotenv import dotenv_values

# Taking values from the env file
username = dotenv_values("env")["TWITTER_USER"] or ""
password = dotenv_values("env")["TWITTER_PASS"] or ""
discord = dotenv_values("env")["DISCORD_WEBHOOK"] or ""

# Run ChatGPT Module
message = input("Type a prompt: ")
system = input("Type how ChatGPT shall process it (insert nothing to ignore): ")
chatgpt = get_response(message, system)
file_to_read = "tmp"
write_input(chatgpt, file_to_read)

# Run Twitter Module
driver = create_driver()
link = post_twitter(driver, file_to_read)

# Run Discord Webhook Module
send_discord(driver, link)
driver.quit()
