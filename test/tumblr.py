#!/usr/bin/env python
from chatbot.chatgpt import get_response
from chatbot.tumblr import post_tumblr

# Run ChatGPT Module
message = input("Type a prompt: ")
system = input("Type how ChatGPT shall process it (insert nothing to ignore): ")
chatgpt = get_response(message, system)
print(chatgpt)

post_url = post_tumblr(message, chatgpt, tag=["API Test"])
print(post_url)
