# Facebook Module
Facebook is a social media created by Mark Zuckerberg in 2004. Users can create status and post photos for friends and family to see and connect with each other. It also offers other features a standard social media has such as groups and pages.

This module uses [Facebook Graph API](https://developers.facebook.com/docs/graph-api/) to interact with many elements offered from Facebook. As Facebook does not allow user account interaction from the API, we can only use a Facebook page to perform such action. No external library is required.

### Environment Variables
- `FB_PAGE_ID`: The Page ID of your Facebook Page
- `FB_PAGE_ACCESS_TOKEN`: The page access token (NOT your user token) from Facebook Graph API

To obtain `FB_PAGE_ACCESS_TOKEN`:
1. Go to [Meta for Developer](https://developers.facebook.com/) webpage and login with your facebook account.
2. Go to My Apps and create an app. **Make sure the app type is "Business" and you can NOT change this later.** Also, your app name can't contain any trademarked name from Meta such as "Face", "Book", "Insta", "Gram", etc.
3. Click "Tools" on the top right of the webpage and choose "Graph API Explorer".
4. [Create a facebook page if you don't have one already](https://www.facebook.com/pages/create/).
5. Change both "Meta App" and "User or Page" to the app you just created. Make sure you allow access to the page you want to use.
6. Edit the "permissions" list like the following. [The list for every permission can be found here](https://developers.facebook.com/docs/permissions).
```
pages_show_list
pages_read_engagement
pages_manage_metadata
pages_read_user_content
pages_manage_posts
pages_manage_engagement
```
7. Now you can generate access token if needed and copy it. This will be the value for `FB_PAGE_ACCESS_TOKEN`. Note that you may need to regenerate it per weekly basis.

To obtain `FB_PAGE_ID`:
1. Go to your page.
2. Click "about" then "Page transparency". You'll see your Page ID number beside a flag icon. This will be the value for `FB_PAGE_ID`.

### Functions
- `post_facebook(message:str) -> str`  

Use a facebook graph API to make a post from `message` to your page and return the ID of the created post. Requires both `FB_PAGE_ACCESS_TOKEN` and `FB_PAGE_ID` in `.env` file.

### Documentation
[Click here to read the full documentation on Facebook Graph API](https://developers.facebook.com/docs/graph-api/)
