# Instagram Module
Instagram is a social media where photos and videos are first class citizen. Users can share photo and apply filters to it as well as geo-tagging said photos. It is used widely by influencers and artists.

This module uses [an unofficial Instagram API called "instagrapi"](https://github.com/subzeroid/instagrapi) to create a post on Instagram. This is done by reverse-engineering Instagram's API to create a library without using Meta's official Instagram Graph API.

# ! WARNING !
Due to its unofficial nature, **do NOT use your main account due to the risk of account suspension.** Only use a secondary or a throwaway account.

### Environment Variables
- `INSTAGRAM_USER`: The username of your Instagram account
- `INSTAGRAM_PASS`: The password of your Instagram account

The values for above are self-explanatory. Note that you are putting your credential in a plain text inside `.env` file. 

### Functions
- `post_instagram(image="", message="", username="", password="") -> str`

Use instagrapi to post photo and caption to your Instagram account. Does NOT require Instagram Graph API. You only need your username and password. Requires both `INSTAGRAM_USER` and `INSTAGRAM_PASS` in `.env` file unless both `username` and `password` are provided directly in the argument.

A location for a valid image is required. If `image` is empty, the system will ask for image location. `image` only accepts file extension of .png, .jpg, and .jpeg at the moment. `message` is for image caption and it's optional.

### Documentation
[Click here to read the full documentation on Instagrapi](https://subzeroid.github.io/instagrapi/)
