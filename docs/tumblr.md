# Name Module
### Environment Variables
- `TUMBLR_BLOG`: The username of your Tumblr account (as well as your blog name)
- `TUMBLR_CONSUMER_KEY`: The Consumer Key of your Tumblr API access
- `TUMBLR_SECRET_KEY`: The Secret Key of your Tumblr API access
- `TUMBLR_OAUTH_TOKEN`: The OAUTH Token of your Tumblr API access
- `TUMBLR_OAUTH_SECRET`: The OAUTH Secret Token of your Tumblr API access
 
To obtain all keys for above variables, perform the following:
1. Login into your Tumblr account
2. [Go to Tumblr's Application Creation Page](https://www.tumblr.com/oauth/apps)
3. Register application, and fill the form. Note that you can fill the url with any url you like (e.g. https://something.com). For OAuth2 redirect URLs, make sure it has "oauth2/authorize" in the url (https://something.com/oauth2/authorize)
4. After creating an application, [Go to Tumblr API Console](https://api.tumblr.com/console/calls/user/info) and you'll see every API key you need in the code section of the page. Paste them into your `.env` file. From top to bottom:
```
TUMBLR_CONSUMER_KEY
TUMBLR_SECRET_KEY
TUMBLR_OAUTH_TOKEN
TUMBLR_OAUTH_SECRET
```

### Functions
- `get_info() -> dict`

Get tumblr user info in json/dictionary format. Requires the four API access keys in `.env` file.

[All the responses under json format can be found here](https://www.tumblr.com/docs/en/api/v2#info---retrieve-blog-info).

- `post_tumblr(title:str, content:str, tag=[], blog="") -> str`

Post a simple Tumblr text and return the url link to the post. Requires the four API access keys as well as `TUMBLR_BLOG` in `.env` file.Blog name can be provided directly in the argument as `blog`.

`title` will be the title of your post while `content` will be the text under said post. `tag` is optional but it must be a list of strings (spaces are allowed inside the strings).

### Documentation
[Click here to read the full documentation on Tumblr API](https://www.tumblr.com/docs/en/api/v2). 
