# Slack Module
Slack is an instant messaging app developed for business and professional people in mind. It offers "workspaces" as the place of where conversation takes place. It is widely used by many companies around the world.

This module uses [Slack's Webhook](https://api.slack.com/automation/triggers/webhook) to create an automated message to a text channel in a workspace. It is one of the feature offered from Slack API and we're using this due to its simplicity. No external library is required.

### Environment Variables
- `SLACK_WEBHOOK`: Slack Webhook URL

To obtain the required URL for the above variable, perform the following:
1. Login into Slack and enter a workplace where you have a permission to manage an app in. This can either be a workplace you created yourself or a workplace where you have an admin priveledge.
2. Go to [this app webpage](https://api.slack.com/apps) and create new app.
3. Create from scratch, give it a name, place it in your desired workplace (you can NOT change this later), and create the app.
4. In "Add Features and Functionality", click "Incoming Webhook" and turn it on.
5. Click "Add New Webhook to Workspace" and choose the channel you want your webhook to post on. You can now copy the webhook URL which is the value for `SLACK_WEBHOOK`.

### Functions
- `send_slack(message:str, webhook_url="") -> int`

Use Slack webhook to send a message to a channel and return its HTTP return code. Requires `SLACK_WEBHOOK` value in `.env` file unless a webhook URL is provided directly under `webhook_url` argument.

### Documentation
[Click here to read the full documentation on Slack's Webhook](https://api.slack.com/messaging/webhooks) as well as [Slack API in general](https://api.slack.com/apis).
