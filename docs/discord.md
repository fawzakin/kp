# Discord Module
Discord is an instant messaging app that allows users to instantly send messages as well as making calls (voice only or with video) with other users around the globe. All communications take place in a "server" where every conversation is grouped under "channel" for specific topic. It is primarily used by gamers but it is also popular for any group that shares interest from fandoms to software developments.

This module uses [Discord's Webhook](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks) to create an automated message to a text channel in a server. As webhook only requires POST method to create a message, no external library is required.

### Environment Variables
- `DISCORD_WEBHOOK`: Discord Webhook URL

To obtain the required URL for the above variable, perform the following:
1. Get into a server where you have a role with "Manage Webhook" permission turned on. You can have this permission from a server you own or a server whose owner/moderator team give you an admin role that may include this permission.
2. Go to server settings, integrations under apps, and view webhooks.
3. Click "New Webhook", click on the newly created webhook, and copy the webhook URL. This will be the value for `DISCORD_WEBHOOK`. Feel free to rename the webhook, give it a profile picture, and put it under a channel for the webhook to operate.

### Functions
- `send_discord(webhook_url="", **kwargs) -> int`

Use Discord webhook to send a message to a channel and return its HTTP return code. Requires `DISCORD_WEBHOOK` value in `.env` file unless a webhook URL is provided directly under `webhook_url` argument.

The supplied parameter for this function is as follow:  
`name`: The name displayed on webhook account.  
`mesg`: The message sent by the webhook account.  
`title`: The title of the embed object (the message inside a box).  
`desc`: The description of the embed object.  

The parameter requires at least one of `mesg`, `title`, or `desc`.  

The following is a sample code:
```python
send_discord(
    name = "Webhook Name",
    mesg = "Webhook Message",
    title = "Webhook Title",
    desc = "Webhook Description"
) # returns 204 if successful
```

This is how the message will look like in a channel:

![webhook](https://cdn.discordapp.com/attachments/1059326079980023838/1175759550729691188/image.png)

### Documentation
[Click here to read the full documentation on Discord's Webhook](https://discord.com/developers/docs/resources/webhook)
