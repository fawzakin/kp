# Mastodon Module
Mastodon is a social media in a similar vain to Twitter with an exception that you can self-host your own instance. Each instance can have its own rule, moderation, and privacy policy but users on any instance can communicate between other instances seamlessly and even move to other instance if desired. 

This module uses [the official Mastodon API](https://docs.joinmastodon.org/client/intro/) to create a post on Mastodon. This module only requires simple POST request and no external library is required.

### Environment Variables
- `MASTODON_INSTANCE`: The URL of a Mastodon Instance your account is currently residing (the place where you created said account).
- `MASTODON_USER`: The username of your Mastodon account.
- `MASTODON_ACCESS_TOKEN`: The Access Token of your application created within your Mastodon account.

To obtain the access token, perform the following:
1. Create an account on a Mastodon instance (e.g. [mastodon.social](https://mastodon.social/))
2. Edit your profile, click Development, and New Application
3. Give it a name and check both `write:media` and `write:statuses` permission. Create the app by clicking "Submit" button.
4. Click on your newly created app and copy your access token. This will be the value for `MASTODON_ACCESS_TOKEN`. 

### Functions
- `post_mastodon(message:str, media_id="", instance="", username="") -> str`

Use official Mastodon API to create a post to your mastodon account. Requires `MASTODON_ACCESS_TOKEN` value in `.env` file. Both `instance` and `username` can be supplied directly here or in `.env` file. 

`message` is required for a post and each instance has different limit on post length (e.g. mastodon.social has 500 characters limit).

`media_id` should be ignored if you are going to create a text post.

- `post_mastodon_photo(image="", message="", instance="", username="") -> str`

This is supposed to post image to Mastodon but it's currently broken. Please don't use this at the moment.

### Documentation
[Click here to read the full documentation on Mastodon API](https://docs.joinmastodon.org/methods/apps/)

