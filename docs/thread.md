# Thread Module
This module converts sentences between set delimiter into each own line inside a text file and turning a text file into a list of strings. This module is primarily used alongside with Twitter Module. 

### Functions
- `write_input(message_input:str, file_output:str, delimiter=". ")`

Write `message_input` into a file in `file_output` line by line. Each sentences between delimiter will be split with newline as the new delimiter.

If the delimiter parameter left as default, each line in the file will be appended a "." at the end.

- `read_input(file_input:str) -> list[str]`
Read a text file in 'file_input' and format it with a "thread" style for Twitter. The function will return a list of string.

The maximum lines that a file can have is 9 lines (excluding empty lines). The maximum characters per line is 274 (280 characters for tweet limit - 6 characters for ' [i/N]' marking at the end)

Example output:
```
Line 1 -> Line 1 [1/3]
Line 2 -> Line 2 [2/3]
Line 3 -> Line 3 [3/3]
```
