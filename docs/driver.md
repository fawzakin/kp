# Driver Module
This module provides a simple wraper for creating a Selenium WebDriver instance.

### Functions
- `create_driver(path=r"/usr/bin/geckodriver", tmp=r"/tmp/log", wait=10) -> WebDriver`

Create a WebDriver instance using geckodriver (Firefox).

The default path is set for Linux system in mind. If you are on other platform (e.g. Windows), you need to manually set `path` to the geckodriver executable and `tmp` to write the log somewhere in your system.

`wait` is the implicit wait for the driver in seconds. If the element in a webpage doesn't exist, the driver will wait for this much second until the element it is looking for is found. Otherwise, it'll throw an exception.

The driver will not quit the browser until close() or quit() is given.

- `create_driver_chrome(path=r"/usr/bin/chromedriver", tmp=r"/tmp/log", wait=10) -> WebDriver`

Create a WebDriver instance using chromedriver (Google Chrome/Chromium).

This function is identical to previous function with the exception that it uses chromedriver as its driver instead of geckodriver. This was done as some website may not function correctly outside of chromium engine or if you just prefer chrome.

### Documentation
[Click here to read the full documentation on Selenium Webdriver](https://www.selenium.dev/documentation/webdriver/). 
