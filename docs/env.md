# Env Module
This module provide a simple wraper for python-dotenv, a library to read environment variable file.

The default name for the env file is `.env`. This can be changed in `ENVFILE` variable inside the module (`env.py`).

### Functions
- `get_env(key: str) -> str:`

A helper function to check if any required value in the env file is missing. It will raise KeyError exception if the variable is not found or if its value is empty.
