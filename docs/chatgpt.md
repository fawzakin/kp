# ChatGPT Module
ChatGPT is a massively popular AI chatbot by OpenAI. It can generate a very convincing response and conversation from any prompt the user writes. It is done by training a machine-learning model with a massive amount of training data. 

This module uses [OpenAI's official python library for ChatGPT](https://pypi.org/project/openai/).

### Environment Variables
- `OPENAI_API_KEY`: API Key for using OpenAI's services (ChatGPT).

To obtain the required API key for the above variable, perform the following:
1. [Go to OpenAI's developer platform website](https://platform.openai.com/) and create an account. Note that you are required to provide them phone number for verification purpose. I know, it sucks. 
2. Hover to the left side of the webpage and click "🔒API Keys".
3. Create new secret key, give it some name, **immediately copy the secret key and paste it somewhere else (e.g. a text file)** as you cannot see the key again once you close the pop-up. This will be the value for `OPENAI_API_KEY`.

Note that you are given free $5.00 credit to try out the API which should be enough

### Functions
- `get_response(message:str, system="", temp=0) -> str`

Return ChatGPT response from message as string. Requires `OPENAI_API_KEY` value in `.env` file.

Parameters:  
`message`: The prompt you want to give to ChatGPT. It will generate response based of the prompt you write.   
`system`: How do you want ChatGPT to handle the message. For example, you can tell it to fix the grammar of the message as the response. Leave this empty if you want ChatGPT to response from the message alone.  
`temp`: How varied the response of ChatGPT would be. Lower value means more consistent result while higher value means more varied result. The range value is 0.0 to 2.0. [Read here for detail](https://platform.openai.com/docs/guides/text-generation/how-should-i-set-the-temperature-parameter).

### Documentation
[Click here to read the full documentation on OpenAI's API](https://platform.openai.com/docs/overview)
