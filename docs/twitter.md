# Twitter Module
Twitter, currently called with its new pretentious name as "X", is a social media where users can create a short message and send photos and videos as a post called a "tweet". The users can also like and share, or "retweet", other people's tweet as well as following others. Twitter is mod

This module uses Selenium Webdriver as an API-free interface to Twitter due to Elon Musk's shenanigans in regards of limiting API access ($100/month for a read access is ridiculous). This module will create a "thread" on a user account from a text file where every line is its own tweet (and it's automatically numbered).

# ! WARNING !
Due to Elon Musk trying to reduce the amount of bots and scrapers from the site, **do NOT use your main account due to the risk of account suspension.** Only use a secondary or a throwaway account.

### Environment Variables
- `TWITTER_USER`: The username of your Twitter account
- `TWITTER_PASS`: The password of your Twitter account

The values for above are self-explanatory. Note that you are putting your credential in a plain text inside `.env` file. 

### Functions
- `post_twitter(driver:WebDriver, path_to_file:str, username="", password="", do_delay=True, delay_time=5) -> str:`

Post a text file as a "thread" to your twitter account. This function will return the link to the first tweet of the thread. Requires both `TWITTER_USER` and `TWITTER_PASS` in `.env` file unless both `username` and `password` are provided directly in the argument.

[Click here to read on how to create the Selenium WebDriver class](driver.md).

A short delay is imposed every time the "Post" button is clicked to ensure that every element is loaded into view. It can be disabled by setting the `do_delay` parameter to false. `delay_time` is in second.

Note that due to current random shenanigans with Elon Musk, this module may not work as intended or your account may get banned. Use at your own risk!

### Documentation
[Click here to read the full documentation on SelenIum Webdriver](https://www.selenium.dev/documentation/webdriver/). 
