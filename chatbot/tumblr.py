from chatbot.env import get_env
from pytumblr import TumblrRestClient

def get_info():
    """Get tumblr user info in json/dictionary format."""
    return get_tumblr_client().info()

def post_tumblr(title:str, content:str, tag=[], blog="") -> str:
    """Post a simple Tumblr text and return the url link to the post.

    'title' will be the title of your post while 'content' will be the text under said post.
    'tag' is optional but it must be a list of strings (spaces are allowed inside the strings).

    if blog isn't specified, the function will fetch
    'TUMBLR_BLOG' from the env file.
    """

    if blog == "":
        blog = get_env("TUMBLR_BLOG")

    if tag == []: 
        post = get_tumblr_client().create_text(
            blogname=blog, 
            state="published",
            title=title,
            body=content,
        )
    else:
        post = get_tumblr_client().create_text(
            blogname=blog, 
            state="published",
            title=title,
            body=content,
            tags=tag,
        )

    try:
        post_id = post["id_string"]
    except KeyError:
        post_error = post["errors"][0]["title"]
        raise ValueError("An error has been encountered: {e}".format(e=post_error))

    return "https://{blog}.tumblr.com/post/{id}".format(blog=blog, id=post_id)

def get_tumblr_client() -> TumblrRestClient:
    """Get the following four environment variables from env
    and return them as TumblrRestClient 
    key     : TUMBLR_CONSUMER_KEY
    key_sec : TUMBLR_SECRET_KEY
    oath    : TUMBLR_OAUTH_TOKEN
    oath_sec: TUMBLR_OAUTH_SECRET
    """
    key      = get_env("TUMBLR_CONSUMER_KEY")
    key_sec  = get_env("TUMBLR_SECRET_KEY")
    oath     = get_env("TUMBLR_OAUTH_TOKEN")
    oath_sec = get_env("TUMBLR_OAUTH_SECRET")

    return TumblrRestClient(key, key_sec, oath, oath_sec)
