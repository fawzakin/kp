from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from chatbot.env import get_env
from requests import post

def send_discord(webhook_url="", **kwargs) -> int:
    """Use Discord webhook to send a message to a channel
    and return HTTP return code.

    Parameter:
        name: The name displayed on webhook account.
        mesg: The message sent by the webhook account.
        title: The title of the embed object (the message inside a box).
        desc: The description of the embed object.

    The parameter requires at least one of mesg, title, or desc.

    if webhook_url isn't specified, the function will fetch
    'DISCORD_WEBHOOK' from the env file.
    """

    if webhook_url == "":
        webhook_url = get_env("DISCORD_WEBHOOK")

    if not ("mesg" in kwargs or "title" in kwargs or "desc" in kwargs):
        raise KeyError("Requires one of 'mesg', 'title', or 'desc'")

    chat = {}
    embed = {}
    if "name" in kwargs:
        chat["username"] = kwargs["name"]
    if "mesg" in kwargs:
        chat["content"] = kwargs["mesg"]
    if "title" in kwargs:
        embed["title"] = kwargs["title"]
    if "desc" in kwargs:
        embed["description"] = kwargs["desc"]
    if embed != {}:
        chat["embeds"] = [embed]

    return post(webhook_url, json=chat).status_code


def send_discord_webdriver(driver:WebDriver, message:str, webhook_url="", wait=30) -> str:
    """Use a discord webhook to send a message to your server
    using Selenium Webdriver. 

    a WebDriver instance is required to be passed in 'driver' parameter.     

    if webhook_url isn't specified, the function will fetch
    'DISCORD_WEBHOOK' from the env file.
    """

    # Initiate the webhook_url
    if webhook_url == "":
        webhook_url = get_env("DISCORD_WEBHOOK")

    # Open the link
    driver.get("https://discohook.org")

    # Clear all text on screen
    driver.find_elements(By.CLASS_NAME, "eRSwdL")[1].click()
    driver.find_elements(By.CLASS_NAME, "olJqx")[-1].click()

    # Type Webhook URL and message
    driver.find_element(By.ID, "_1_url").send_keys(webhook_url)
    driver.find_element(By.ID, "_9_content").send_keys(message)

    # Click on the send button once it gets highlighted
    send_button = driver.find_elements(By.CLASS_NAME, "olJqx")[0]
    wait = WebDriverWait(driver, wait)
    wait.until(expected_conditions.element_to_be_clickable(send_button))
    send_button.click()

    return "OK"
