from pathlib import Path

EXT = [".png", ".jpg", ".jpeg"]

def get_image_path(name:str, image="") -> Path:
    """Get the image path and check its validity.

    Parameters:
        image: Path to image. This function will ask for manual input if it's empty.
        name: Used to identify what platform the image is going to be uploaded for when an input is asked.
    """
    
    if image == "":
        image = input(f"Relative path to image for {name}: ")
    im = Path(image)
    test_image_path(im)
    return im

def test_image_path(file:Path):
    """Check if the file input a valid image file."""

    if not file.is_file():
        raise FileNotFoundError(f"File {file} doesn't exist.")
    elif file.suffix not in EXT:
        raise FileExistsError(f"File {file} isn't part of supported image file (png, jpg, and jpeg)")
