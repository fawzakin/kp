class Options:
    """Convert option list from getopt into dictionary."""

    def __init__(self, opts:list[tuple[str, str]]) -> None:
        """Creates a new instance for Option class."""
        
        self.args_dict = self.__create_dict(opts)
        pass

    def __create_dict(self, opts: list[tuple[str, str]]) -> dict[str, str]:
        args_dict = {}
        for args in opts:
            args_dict[args[0].strip()] = args[1].strip()
        return args_dict

    def opt(self, option: str) -> bool:
        """Check if such option exists"""
        if option in self.args_dict:
            return True
        return False

    def arg(self, option: str) -> str:
        """Return argument from option if such option exists"""
        return self.args_dict[option] if self.opt(option) else ""
