from chatbot.env import get_env
from chatbot.photo import get_image_path
from requests import post
from pathlib import Path

def post_mastodon(message:str, media_id="", instance="", username="") -> str:
    """Use official Mastodon API to create a post to your Mastodon account.

    'message' is required for a post and each instance has different limit on post length (e.g. mastodon.social has 500 characters limit).

    'media_id' should be ignored if you are only going to create a text post.

    if 'instace' and 'username' aren't specified, the function will fetch
    'MASTODON_INSTANCE' and 'MASTODON_USERNAME' from the env file.
    """
    instance, username = get_mastodon_env(instance, username)
    head = get_mastodon_header()

    if media_id != "":
        form = {
            "status": message,
            "media_ids": [media_id]
        }
    elif message != "":
        form = { "status": message }
    else:
        raise ValueError("Message cannot be empty")

    url = f"https://{instance}/api/v1/statuses"
    res = post(url, data=form, headers=head).json()
    get_error(res)

    id = res["id"]
    return f"https://{instance}/{username}/{id}"

def post_mastodon_photo(image="", message="", instance="", username="") -> str:
    """This is supposed to post image to Mastodon but it's currently broken.
    Please don't use this at the moment.
    """
    instance, username = get_mastodon_env(instance, username)
    head = get_mastodon_header()

    # Get image and check its validity
    im = get_image_path("Mastodon", image)
    
    # Upload Image
    url = f"https://{instance}/api/v2/media"
    id =""
    with open(im, "rb") as img:
        form = { "file": (im.name, img, 'multipart/form-data')}
        print(form)
        res = post(url, data=form, headers=head).json()
        get_error(res)
        id = res["id"]
    pass

    print(id)
    return post_mastodon(message, id, instance, username)

def get_mastodon_env(instance:str, username:str) -> tuple[str, str]:
    """Get the environment variable MASTODON_INSTANCE and MASTODON_USER from env"""
    if instance == "":
        instance = get_env("MASTODON_INSTANCE")

    if username == "":
        username = get_env("MASTODON_USER")
        if not username.startswith("@"):
            username = "@" + username

    return instance, username

def get_mastodon_header() -> dict[str, str]:
    """Get the enviroment variable MASTODON_ACCESS_TOKEN from env"""
    key = get_env("MASTODON_ACCESS_TOKEN")
    return { 'Authorization': f'Bearer {key}' }

def get_error(res:dict):
    if "error" in res:
        err = res["error"]
        raise ValueError(f"An error has occured: {err}")
