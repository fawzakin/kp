from chatbot.env import get_env
from urllib.parse import quote
from requests import post

def post_facebook(message:str):
    """Use a facebook graph API to make a post from `message`
    to your page and return the ID of the created post.

    Requires your page ID and page access token from your app
    as FB_PAGE_ACCESS_TOKEN and FB_PAGE_ID in env file.
    """

    fb_id  = get_env("FB_PAGE_ID")
    fb_api = get_env("FB_PAGE_ACCESS_TOKEN")
    url = "https://graph.facebook.com/v18.0/{id}/feed?message={m}&access_token={t}".format(
        id = fb_id,
        m = quote(message),
        t = fb_api
    )
    url_post = post(url).json()
    if "id" in url_post:
        return url_post["id"]
    elif "error" in url_post:
        return "An error has occured: " + url_post["error"]["message"]
