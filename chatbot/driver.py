from selenium import webdriver

def create_driver_firefox(path=r"/usr/bin/geckodriver", tmp=r"/tmp/log", wait=10) -> webdriver.Firefox:
    """Create a WebDriver instance using geckodriver (Firefox). 

    The default path is set for Linux system in mind. If you are on other platform
    (e.g. Windows), you need to manually set `path` to the geckodriver executable 
    and `tmp` to write the log somewhere in your system.

    `wait` is the implicit wait for the driver in seconds. If the element in a 
    webpage doesn't exist, the driver will wait for this much second until the 
    element it is looking for is found. Otherwise, it'll throw an exception.

    The driver will not quit the browser until close() or quit() is given.
    """

    service = webdriver.FirefoxService(executable_path=path, log_output=tmp)
    driver = webdriver.Firefox(service=service)
    driver.implicitly_wait(wait)

    return driver

def create_driver_chrome(path=r"/usr/bin/chromedriver", tmp=r"/tmp/log", wait=10) -> webdriver.Chrome:
    """Create a WebDriver instance using chromedriver (Google Chrome/Chromium).

    The default path is set for Linux system in mind. If you are on other platform
    (e.g. Windows), you need to manually set `path` to the geckodriver executable 
    and `tmp` to write the log somewhere in your system.

    `wait` is the implicit wait for the driver in seconds. If the element in a 
    webpage doesn't exist, the driver will wait for this much second until the 
    element it is looking for is found. Otherwise, it'll throw an exception.

    The driver will not quit the browser until close() or quit() is given.
    """

    options = webdriver.ChromeOptions()
    options.add_experimental_option("detach", True)
    service = webdriver.ChromeService(executable_path=path, log_output=tmp)
    driver = webdriver.Chrome(service=service, options=options)
    driver.implicitly_wait(wait)

    return driver
