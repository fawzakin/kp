from chatbot.env import get_env
from requests import post

def send_slack(message:str, webhook_url="") -> int:
    """Use Slack webhook to send a message to a channel
    in your workspace and return its HTTP return code.

    if 'webhook_url' isn't specified, the function will fetch
    'SLACK_WEBHOOK' from the env file.
    """
    if webhook_url == "":
        webhook_url = get_env("SLACK_WEBHOOK")
    message_input = {"text": "{msg}".format(msg=message)}
    return post(webhook_url, json=message_input).status_code # Returns HTML Return Code
