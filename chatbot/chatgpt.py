from chatbot.env import get_env
from openai import OpenAI

def get_response(message:str, system="", temp=0):
    """Return ChatGPT response from message as string.

    Parameter:
        - message: The prompt for ChatGPT [Required].
        - system: How ChatGPT handle the prompt.
        - temp: How varied the output is (0.0 - 1.0).
    """

    client = OpenAI(api_key=get_env("OPENAI_API_KEY"))

    message_input = []
    user_input = {
        "role": "user",
        "content": "{msg}".format(msg=message)
    }
    if system != "":
        syst_input = {
            "role": "system",
            "content": "{msg}".format(msg=system)
        }
        message_input.append(syst_input)
    message_input.append(user_input)

    message_output = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=message_input,
        temperature=temp
    )
     
    return message_output.choices[0].message.content
