from chatbot.env import get_env
from chatbot.photo import get_image_path
from instagrapi import Client
from pathlib import Path

def post_instagram(image="", message="", username="", password="") -> str:
    """Use instagrapi to post photo and caption to your Instagram account.
    Does NOT require Instagram Graph API. You only need your username and password.

    if image is empty, the system will ask for image location.

    if username and password aren't specified, the function will fetch
    'INSTAGRAM_USER' and 'INSTAGRAM_PASS' from the env file.
    """
    if username == "":
        username = get_env("INSTAGRAM_USER")
    if password == "":
        password = get_env("INSTAGRAM_PASS")

    im = get_image_path("Instagram", image)

    c = Client()
    c.login(username, password)
    p = c.photo_upload(im, message)
    return p.json()
