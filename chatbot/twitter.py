from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.by import By
from chatbot.env import get_env 
from chatbot.thread import read_input
from time import sleep
from math import floor

def post_twitter(driver:WebDriver, path_to_file:str, username="", password="", do_delay=True, delay_time=5) -> str:
    """Post a text file as a "thread" to your twitter account.
    This function will return the link to the first tweet of the thread.

    A short delay is imposed every time the "Post" button is clicked
    to ensure that every element is loaded into view.
    It can be disabled by setting the do_delay parameter to false.

    if username and password aren't specified, the function will fetch
    'TWITTER_USER' and 'TWITTER_PASS' from the env file.

    Note that due to current random shenanigans with Elon Musk, 
    this module may not work as intended or your account may get banned.
    Use at your own risk!
    """
    # Initiate username and password
    if username == "":
        username = get_env("TWITTER_USER")
    if password == "":
        password = get_env("TWITTER_PASS")

    # Read thread file and open the link
    thread = read_input(path_to_file)
    driver.get("https://twitter.com/i/flow/login")

    # Login
    driver.find_element(By.CSS_SELECTOR, "input[autocomplete='username']").send_keys(username)
    for button in driver.find_elements(By.CSS_SELECTOR, "div[role='button']"):
        if button.text == "Next": button.click(); break # The location for the next button varied. We have to use for loop.
    driver.find_element(By.CSS_SELECTOR, "input[autocomplete='current-password']").send_keys(password)
    driver.find_element(By.CSS_SELECTOR, "div[data-testid='LoginForm_Login_Button']").click()

    # Post the first text in the link
    driver.find_element(By.CSS_SELECTOR, "div[role='textbox']").send_keys(thread[0])
    driver.find_element(By.CSS_SELECTOR, "div[data-testid='tweetButtonInline']").click()
    sleep_timer(delay_time, "Posting Tweet #1", do_delay)
    print("")
    
    # Get to the first tweet
    driver.get("https://twitter.com/{user}".format(user=username))
    latest_tweets = driver.find_elements(By.CSS_SELECTOR, "article[data-testid='tweet']")
    latest_tweets[0].click()
    thread_link = driver.current_url

    # Click on the first tweet and perform a loop for the rest of the threads
    num = 1
    while num < len(thread):
        driver.find_element(By.CSS_SELECTOR, "div[role='textbox']").send_keys(thread[num])
        sleep_timer(floor(delay_time/2), is_true=do_delay)
        driver.find_element(By.CSS_SELECTOR, "div[data-testid='tweetButtonInline']").click()
        num += 1
        sleep_timer(delay_time, "Posting Tweet #{num}".format(num=num), do_delay)
        driver.find_elements(By.CSS_SELECTOR, "article[data-testid='tweet']")[-1].click()
        sleep_timer(floor(delay_time/2), is_true=do_delay)
     
    # Like the final tweet in the thread and return the thread's link
    driver.find_elements(By.CSS_SELECTOR, "div[data-testid='like']")[-1].click()
    return thread_link

def sleep_timer(second, message="", is_true=True):
    """Custom sleep wraper with cool message.

    If message is empty, this function will act like regular sleep command.
    Set is_true to false to disable this function entirely.
    """

    if not is_true:
        pass
    if message == "":
        sleep(second)
    else:
        print(message)
        for x in range(1,second+1):
            print(x, end="...", flush=True)
            sleep(1)
    print("")
    pass

