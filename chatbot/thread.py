def write_input(message_input:str, file_output:str, delimiter=". "):
    """Write 'message_input' into a file in 'file_output' line by line.
    Each sentences between delimiter will be split with newline as the new delimiter.

    If the delimiter parameter left as default, 
    each line in the file will be appended a "." at the end.
    """
    messages = message_input.split(delimiter)
    with open(file_output, 'w') as f:
        for message in messages:
            if delimiter == ". " and not message.endswith("."):
                message += "."
            f.write(f"{message}\n")


def read_input(file_input:str) -> list[str]:
    """Read a text file in 'file_input' and format it with a "thread" style for Twitter.
    The function will return a list of string.

    The maximum lines that a file can have is 9 lines (excluding empty lines).
    The maximum characters per line is 274 (280 characters for tweet limit - 6 characters for ' [i/N]' marking at the end)

    Example output:
    Line 1    Line 1 [1/3]
    Line 2 -> Line 2 [2/3]
    Line 3    Line 3 [3/3]
    """
    try:
        f = open(file_input, "r")
    except:
        raise IOError("The file '{f}' is not found.".format(f=file_input))

    # Clean empty line
    raw_lines = f.read().split("\n")
    clean_lines = []
    for raw in raw_lines:
        if len(raw) != 0:
            clean_lines.append(raw)

    if len(clean_lines) == 1:
        return clean_lines
    elif len(clean_lines) > 9:
        raise ValueError("The file '{f}' contains more than 9 lines.".format(f=file_input))

    # Format all lines for Twitter Thread
    page_number = 1
    output_lines = []
    for clean in clean_lines:
        if len(clean) > 274:
            raise ValueError("This line is too long: {s} (max: 274 lines)".format(s=clean))
        page = " [" + str(page_number) + "/" + str(len(clean_lines)) + "]"
        page_number += 1
        output_lines.append(clean + page)

    return output_lines
