# Browser Army Modules (Python 3)
This repository contains all modules, written in Python 3, made for Versatile ChatBot Swarm project, a research project aimed for creating an automated social media bot, powered by ChatGPT, with massive scalability as its central topic. 

Part of my work practice (kerja praktek) as a research assistant.

WARNING: Use this script at your own risk. I am NOT responsible for any of your social media account to be suspended due to suspicious automated activity. Please only use a throwaway account to play around with this modules.

# Table of Content
- [Browser Army Modules](#browser-army-modules)
- [Table of Content](#table-of-content)
- [About](#about)
    - [Overview](#overview)
    - [Purpose](#purpose)
- [Quick Start](#quick-start)
    - [Dependencies](#dependencies)
    - [Environment Variables](#environment-variables)
    - [Using the Modules](#using-the-modules)
- [Social Media Modules](#)
    - [ChatGPT](docs/chatgpt.md)
    - [Discord](docs/discord.md)
    - [Facebook](docs/facebook.md)
    - [Instagram](docs/instagram.md)
    - [Mastodon](docs/mastodon.md)
    - [Slack](docs/slack.md)
    - [Tumblr](docs/tumblr.md)
    - [Twitter](docs/twitter.md)
- [Other Modules](#)
    - [driver](docs/driver.md)
    - [env](docs/env.md)
    - [thread](docs/thread.md)
- [License](#license)

# About
### Overview
The Browser Army Modules (Bromy) repository contains multiple modules made for ChatGPT and many popular social media written in Python 3. The modules take advantage of multiple form of interfaces to interact with the target social media, from using the official or unofficial API/library, a simple webhook, to Selenium Driver as an API-free interface (with the most risk of account suspension). 

The way the modules work are divided by three parts:
- ChatGPT Module: This module is essentially a wrapper to OpenAI's official library for inputting prompt for ChatGPT and receiving its response as a string in a single function.
- Social Media Module: Each module for every social media currently targeted so far is written for creating a simple post to an account made accessible from the [environment variables](#environment-variables).
- Helper Module: Miscellaneous modules for other modules to use. 

### Purpose
The purpose of this repository is to provide an easy interface (for the main project researchers) to interact with ChatGPT and social media. There are many use cases this research project is aimed for. One of its use case which is also the project's scope is an automated customer service bot that uses ChatGPT to answer user's question and problem without any human intervention in large scale and short time span.

### Aim and Scope
This project aims to create a simple abstraction for accessing ChatGPT and Multiple Social Media. My current scope is to write a basic interface for ChatGPT as well as a simple interface for posting a text post onto target social media. All modules are writen in Python 3 for simplicity and readability.

# Quick Start
The following is the steps to get the browser army modules working. TL;DR:
1. Install the dependencies
2. Fill the environment variable file (`.env` file)
3. Import the module (`chatbot.py` is provided as a sample code)

### Dependencies
Before using any module, installing all dependencies is needed for all modules to function. All dependencies have been included in `requirements.txt` and can be installed using the command below:
```
python3 -m venv .pyenv
source .pyenv/bin/activate
pip install -r requirements.txt
```
Make sure to run `source start` every time a new terminal session is made before fiddling around with the modules.

The following is the list for the main dependencies included in `requirements.txt`:
- `instagrapi`: Instagram module
- `openai`: ChatGPT module
- `python-dotenv`: All module to read the env file
- `pytumblr`: Tumblr module
- `selenium`: Twitter and Discord Webhook modules

### Environment Variables
The `.env` file contains every value needed to let all the scripts function. The values are the API keys, Username/Password combo, or Webhook url for ChatGPT and Social Media.

The following is the list of every environment variables the modules will get from. It is grouped by which service the variable is used for. Please refer to the service's specific documentation to learn how to get its values.
- [ChatGPT](docs/chatgpt.md)
    - `OPENAI_API_KEY`: API Key for using OpenAI's services (ChatGPT).
- [Discord](docs/discord.md)
    - `DISCORD_WEBHOOK`: Discord Webhook Url
- [Facebook](docs/facebook.md)
    - `FB_PAGE_ID`: The Page ID of your Facebook Page
    - `FB_PAGE_ACCESS_TOKEN`: The Page access token (NOT your user token) from Facebook Graph API
- [Instagram](docs/instagram.md)
    - `INSTAGRAM_USER`: The username of your Instagram account
    - `INSTAGRAM_PASS`: The password of your Instagram account
- [Mastodon](docs/mastodon.md)
    - `MASTODON_INSTANCE`: The URL of a Mastodon Instance your account is currently residing.
    - `MASTODON_USER`: The username of your Mastodon account
    - `MASTODON_ACCESS_TOKEN`: The Access Token of your application created within your Mastodon account.
- [Slack](docs/slack.md)
    - `SLACK_WEBHOOK`: Slack Webhook URL
- [tumblr](docs/tumblr.md)
    - `TUMBLR_BLOG`: The username of your Tumblr account (as well as your blog name)
    - `TUMBLR_CONSUMER_KEY`: The Consumer Key of your Tumblr API access
    - `TUMBLR_SECRET_KEY`: The Secret Key of your Tumblr API access
    - `TUMBLR_OAUTH_TOKEN`: The OAUTH Token of your Tumblr API access
    - `TUMBLR_OAUTH_SECRET`: The OAUTH Secret Token of your Tumblr API access
- [twitter](docs/twitter.md)
    - `TWITTER_USER`: The username of your Twitter account
    - `TWITTER_PASS`: The password of your Twitter account

### Using the Modules
`chatbot.py` is a sample script to run and test all the modules available in this project. It is done by inputting a prompt for ChatGPT and posting its output into any social media of your choosing. Run `--help` or `-h` to see all available options.

For example, if you want ChatGPT to create a post for both Tumblr and Twitter at the same time, run the script like this:
```bash
python3 ./chatbot.py --tumblr --twitter
```

When the script is run, the script will ask for an input for both ChatGPT prompt and system. This can be skipped by inputting both prompt and system directly with `--prompt <your input>` and `--system <your input>` (if `--prompt` is provided, `--system` is optional and it won't be asked during run time). The output of ChatGPT response will also be printed into stdout and written to `tmp` in case there's something wrong with the social media module.

`--skip` and `--skip-message <message>` will completely skip the ChatGPT portion to save credits during testing. The former will simply output "Sample Text" while the latter output whatever `<message>` is given.

Other options for each social media are explained in each respective service's documentation.

You are encouraged to create your own driver script for your own need as `chatbot.py` is written more-less as an example script on the usage of Bromy modules in general.

# License
GNU GPL Version 3.
