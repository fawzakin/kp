#!/usr/bin/env python
# This is a sample code to integrate all currently implemented modules

import sys
import getopt 
from chatbot.chatgpt import get_response
from chatbot.discord import send_discord, send_discord_webdriver
from chatbot.driver import create_driver_firefox
from chatbot.env import get_env
from chatbot.facebook import post_facebook
from chatbot.instagram import post_instagram
from chatbot.mastodon import post_mastodon, post_mastodon_photo
from chatbot.options import Options
from chatbot.slack import send_slack
from chatbot.thread import write_input
from chatbot.tumblr import post_tumblr
from chatbot.twitter import post_twitter

PROMPT = "Type a prompt: "
SYSTEM = "Type how ChatGPT shall process it (insert nothing to ignore): "
OPTION = [
    "help",
    "prompt =",
    "system =",
    "skip-message =",
    "skip",
    "discord",
    "discord-webdriver",
    "facebook",
    "instagram",
    "mastodon",
    "mastodon-image",
    "slack",
    "tumblr",
    "twitter",
]

def get_help():
    help = []
    for op in OPTION:
        if op.endswith("="):
            op = op[:-1] + "[Input]"
        help.append("--" + op)
    print("Available options:")
    for op in help:
        print(op, end=" ")
    print("")

def main(argv):
    try:
        opts = getopt.getopt(argv, "h", OPTION)[0]
        o = Options(opts)
    except getopt.GetoptError:
        print("Unknown option\nRun 'sample.py -h' to see available options.")
        sys.exit(1)

    # Print help if it exists then quit
    if o.opt("-h") or o.opt("--help"):
        get_help()
        sys.exit(0)

    # Run ChatGPT before inserting them to other functions
    if not o.opt("--skip") and not o.opt("--skip-message"):
        get_env("OPENAI_API_KEY")
        prompt = o.arg("--prompt") if o.opt("--prompt") else input(PROMPT)
        if o.opt("--system"):
            system = o.arg("--system")
        elif o.opt("--prompt") and not o.opt("--system"):
            system = ""
        else:
            system = input(SYSTEM)
        chatgpt = get_response(prompt, system)
        chatgpt = chatgpt if chatgpt != None else ""
    else:
        prompt = "Sample Prompt"
        system = ""
        chatgpt = "Sample Text"
        if o.opt("--skip-message"):
            chatgpt = o.arg("--skip-message")

    print(chatgpt)
    write_input(chatgpt, "tmp")

    # Run Modules depending on what options has been given.
    # Not giving options at all would just run chatgpt
    for p in o.args_dict:
        if p == "--discord":
            print("Running Discord Module...")
            print(send_discord(title=prompt, desc=chatgpt))
        elif p == "--discord-webdriver":
            print("Running Discord Webdriver Module...")
            driver = create_driver_firefox()
            print(send_discord_webdriver(driver, chatgpt))
            driver.quit()
        elif p == "--slack":
            print("Running Slack Module...")
            print(send_slack(chatgpt))
        elif p == "--tupost_instagrammblr":
            print("Running Tumblr Module...")
            print(post_tumblr(prompt, chatgpt))
        elif p == "--twitter":
            print("Running Twitter Module...")
            driver = create_driver_firefox()
            print(post_twitter(driver, "tmp"))
            driver.quit()
        elif p == "--facebook":
            print("Running Facebook Page Module...")
            print(post_facebook(chatgpt))
        elif p == "--instagram":
            print("Running Facebook Page Module...")
            print(post_instagram("test.jpg", chatgpt))
        elif p == "--mastodon":
            print("Running Mastodon Module...")
            print(post_mastodon(chatgpt))
        elif p == "--mastodon-image":
            print("Running Mastodon Image Module...")
            print(post_mastodon_photo("test.jpg", chatgpt))

if __name__ == "__main__":
    main(sys.argv[1:])
